# ciclo-de-vida.js

class MiComponente extends Component {

	constructor(){
	  // Enlazo (bind) eventos y/o inicializo estado
	}
  
	componentWillMount(){
	  // Se ejecuta antes de montar el componente
	  // Se podría usar para hacer un setState()
	}
  
	render(){
	  //